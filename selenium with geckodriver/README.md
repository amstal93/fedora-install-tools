I found out that a lot of people have struggles with installation process of selenium & geckodriver. So There are 3 steps how can you do it.

- use dockerfile from this repo
- download .sh script and execute it
- or take the steps bellow (manually)

## Install selenium with geckodriver on Fedora linux

Let's assume, that you have python & pip already installed. Start with download of firefox from official fedora repository:
 
    dnf install -y firefox

Download selenium for python:

    pip install selenium

Now, download the geckodriver and "unzip it":

    wget https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz
    tar -xvf geckodriver-v0.30.0-linux64.tar.gz

Move geckodriver to usr/bin:

    mv geckodriver /usr/bin/

Last step is to make geckodriver executable:

    chmod +x /usr/bin/geckodriver

And that's It :)

You also can use Dockerfile from this repo.
